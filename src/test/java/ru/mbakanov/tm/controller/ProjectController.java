package ru.mbakanov.tm.controller;

import ru.mbakanov.tm.api.service.IAuthService;
import ru.mbakanov.tm.api.service.IProjectService;
import ru.mbakanov.tm.error.ObjectNotFoundExeption;
import ru.mbakanov.tm.entity.Project;
import ru.mbakanov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements ru.mbakanov.tm.api.controller.IProjectController {

    private final IProjectService projectService;

    private final IAuthService authService;

    public ProjectController(IProjectService projectService, IAuthService authService) {
        this.projectService = projectService;
        this.authService = authService;
    }

    final short commandAccessLevel = 1;

    @Override
    public void showProjects() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[LIST PROJECTS]");
        final String userId = authService.getUserId();
        final List<Project> projects = projectService.findAll(userId);
        int index = 1;
        for (Project project: projects) {
            System.out.println(index + ". " +  project);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[CLEAR PROJECTS]");
        final String userId = authService.getUserId();
        projectService.clean(userId);
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[CREATE PROJECT]");
        System.out.println("[enter name:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[enter description:]");
        final String description = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        projectService.create(userId, name, description);
        System.out.println("[OK]");
    }

    private void showProject(final Project project) {
        authService.checkUserAccess(commandAccessLevel);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
    }

    @Override
    public void showProjectById() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[SHOW PROJECT]");
        System.out.println("[enter id]");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Project project = projectService.findOneById(userId, id);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        System.out.println("[OK]");
        showProject(project);
    }

    @Override
    public void showProjectByName() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[SHOW PROJECT]");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Project project = projectService.findOneByName(userId, name);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        System.out.println("[OK]");
        showProject(project);
    }

    @Override
    public void showProjectByIndex() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[SHOW PROJECT]");
        System.out.println("[enter index]");
        final Integer index = TerminalUtil.nextNumber();
        final String userId = authService.getUserId();
        final Project project = projectService.findOneByIndex(userId, index);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        System.out.println("[OK]");
        showProject(project);
    }

    @Override
    public void updateProjectById() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[enter id]");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Project project = projectService.findOneById(userId, id);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[enter description]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdater = projectService.updateOneById(userId, id, name, description);
        if (projectUpdater == null) throw new ObjectNotFoundExeption("Updated project");
        System.out.println("[OK]");
        showProject(projectUpdater);
    }

    @Override
    public void updateProjectByIndex() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[enter index]");
        final Integer index = TerminalUtil.nextNumber();
        final String userId = authService.getUserId();
        final Project project = projectService.findOneByIndex(userId, index);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[enter description]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdater = projectService.updateOneByIndex(userId, index, name, description);
        if (projectUpdater == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
        showProject(projectUpdater);
    }

    @Override
    public void removeProjectById() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[enter id]");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Project project = projectService.removeOneById(userId, id);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Project project = projectService.removeOneByName(userId, name);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[enter index]");
        final Integer index = TerminalUtil.nextNumber();
        final String userId = authService.getUserId();
        final Project project = projectService.removeOneByIndex(userId, index);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        else System.out.println("[OK]");
    }
}
