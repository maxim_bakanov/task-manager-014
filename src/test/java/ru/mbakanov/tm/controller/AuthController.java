package ru.mbakanov.tm.controller;

import ru.mbakanov.tm.api.controller.IAuthController;
import ru.mbakanov.tm.api.service.IAuthService;
import ru.mbakanov.tm.util.TerminalUtil;

public class AuthController implements IAuthController {

    private final IAuthService authService;

    public AuthController(final IAuthService authService) {
        this.authService = authService;
    }

    @Override
    public void login() {
        final short commandAccessLevel = 2;
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[LOGIN]");
        System.out.println("[Enter user login]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[Enter user password]");
        final String password = TerminalUtil.nextLine();
        authService.login(login, password);
        System.out.println("[OK]");
    }

    @Override
    public void logout() {
        final short commandAccessLevel = 1;
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[LOGOUT]");
        authService.logout();
        System.out.println("[OK]");
    }

    @Override
    public void registry() {
        final short commandAccessLevel = 0;
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[REGISTRY]");
        System.out.println("[Enter user login]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[Enter user password]");
        final String password = TerminalUtil.nextLine();
        System.out.println("[Enter user e-mail]");
        final String email = TerminalUtil.nextLine();
        authService.registry(login, password, email);
        System.out.println("[OK]");
    }

}
