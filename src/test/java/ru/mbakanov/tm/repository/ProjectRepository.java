package ru.mbakanov.tm.repository;

import ru.mbakanov.tm.error.ObjectNotFoundExeption;
import ru.mbakanov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements ru.mbakanov.tm.api.repository.IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    @Override
    public void add(String userId, Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public void remove(String userId, Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll(String userId) {
        return projects;
    }

    @Override
    public void clean(String userId) {
        projects.clear();
    }

    @Override
    public Project findOneById(String userId, final String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByName(String userId, final String name) {
        for (final Project project : projects) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(String userId, final Integer index) {
        return projects.get(index-1);
    }

    @Override
    public Project removeOneById(String userId, final String id) {
        final Project project = findOneById(userId, id);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeOneByName(String userId, final String name) {
        final Project project = findOneByName(userId, name);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeOneByIndex(String userId, final Integer index) {
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        remove(userId, project);
        return project;
    }

}
