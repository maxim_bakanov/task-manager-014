package ru.mbakanov.tm.repository;

import ru.mbakanov.tm.api.repository.IUserRepository;
import ru.mbakanov.tm.entity.User;
import ru.mbakanov.tm.error.ObjectNotFoundExeption;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(final String id) {
        for (final User user: users) {
            if (id.equals(user.getId())) return user;
        }
        throw new  ObjectNotFoundExeption("User");
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user: users) {
            if (login.equals(user.getLogin())) return user;
        }
        throw new  ObjectNotFoundExeption("User");
    }

    @Override
    public User removeUser(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User removeById(final String id) {
        final User user = findById(id);
        if (user == null) throw new  ObjectNotFoundExeption("User");
        return removeUser(user);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) throw new  ObjectNotFoundExeption("User");
        return removeUser(user);
    }

    @Override
    public User updateNameById(final String id, final String firstName, final String middleName, final String lastName) {
        Integer index = 0;
        for (User user : users) {
            if (user.getId().equals(id)) {
                user.setFirstName(firstName);
                user.setMiddleName(middleName);
                user.setLastName(lastName);
                users.set(index, user);
                return user;
            }
        }
        throw new  ObjectNotFoundExeption("User");
    }

    @Override
    public User updateEmailById(final String id, final String email) {
        Integer index = 0;
        for (User user : users) {
            if (user.getId().equals(id)) {
                user.setEmail(email);
                users.set(index, user);
                return user;
            }
        }
        throw new  ObjectNotFoundExeption("User");
    }

    @Override
    public User updatePasswordById(String id, String passwordHash) {
        Integer index = 0;
        for (User user : users) {
            if (user.getId().equals(id)) {
                user.setPasswordHash(passwordHash);
                users.set(index, user);
                return user;
            }
        }
        throw new  ObjectNotFoundExeption("User");
    }
}
