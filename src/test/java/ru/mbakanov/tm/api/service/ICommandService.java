package ru.mbakanov.tm.api.service;

import ru.mbakanov.tm.dto.Command;

public interface ICommandService {

    Command[] getCommands(short access);

    String[] getArgs();

    Command[] getTerminalCommands();

}
