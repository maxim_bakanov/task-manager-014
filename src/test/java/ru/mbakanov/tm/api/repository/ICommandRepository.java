package ru.mbakanov.tm.api.repository;

import ru.mbakanov.tm.dto.Command;

public interface ICommandRepository {

    String[] getArgs (Command... values);

    Command[] getCommands(short access);

    String[] getArgs();

    Command[] getTerminalCommands();

}
