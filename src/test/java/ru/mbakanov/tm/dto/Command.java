package ru.mbakanov.tm.dto;

public class Command {

    private String name = "";

    private String arg = "";

    private String description = "";

    private short access = 0; //метка уровня доступа, 0-admin 1-user 2-all

    public Command(String name, String arg, String description, short access) {
        this.name = name;
        this.arg = arg;
        this.description = description;
        this.access = access;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public short getAccess() {
        return access;
    }

    public void setAccess(short access) {
        this.access = access;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        if (name != null && !name.isEmpty()) result.append(name);
        if (arg != null && !arg.isEmpty()) result.append(", ").append(arg);
        if (description != null && !description.isEmpty()) result.append(": ").append(description);
    /*    if (access == 0) result.append(" - access: admin");
        if (access == 1) result.append(" - access: user");
        if (access == 2) result.append(" - access: all");   */
        return result.toString();
    }

}
