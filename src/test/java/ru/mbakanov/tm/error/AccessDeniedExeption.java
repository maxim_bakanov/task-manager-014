package ru.mbakanov.tm.error;

public class AccessDeniedExeption extends RuntimeException {

    public AccessDeniedExeption() {
        super("Error! Access denied...");
    }

}
