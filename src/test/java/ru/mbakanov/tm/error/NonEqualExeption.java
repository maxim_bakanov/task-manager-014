package ru.mbakanov.tm.error;

public class NonEqualExeption extends RuntimeException {

    public NonEqualExeption(String value){
        super("Error! " + value + " is not equal...");
    }

}
