package ru.mbakanov.tm.service;

import ru.mbakanov.tm.api.repository.ICommandRepository;
import ru.mbakanov.tm.api.service.ICommandService;
import ru.mbakanov.tm.dto.Command;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getCommands(short access) {
        return commandRepository.getCommands(access);
    }

    public String[] getArgs() {
        return commandRepository.getArgs();
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
