package ru.mbakanov.tm.service;

import ru.mbakanov.tm.api.service.IAuthService;
import ru.mbakanov.tm.api.service.IUserService;
import ru.mbakanov.tm.entity.User;
import ru.mbakanov.tm.error.AccessDeniedExeption;
import ru.mbakanov.tm.error.ParamEmptyExeption;
import ru.mbakanov.tm.role.Role;
import ru.mbakanov.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedExeption();
        return userId;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new ParamEmptyExeption("Login");
        if (password == null || password.isEmpty()) throw new ParamEmptyExeption("Password");
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedExeption();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedExeption();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedExeption();
        userId = user.getId();
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new ParamEmptyExeption("Login");
        if (password == null || password.isEmpty()) throw new ParamEmptyExeption("Password");
        if (email == null || email.isEmpty()) throw new ParamEmptyExeption("Email");
        userService.create(login, password, email);
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public short getUserAccess() {
        short access = 2;
        if (userId == null) {
            access = 2;
            return access;
        }
        final Role role = userService.findById(userId).getRole();
        final String accessRole = role.getDisplayName();
        if (accessRole.equals("Пользователь")) access = 1;
        if (accessRole.equals("Администратор")) access = 0;
        return access;
    }

    @Override
    public void checkUserAccess(short accessLevel) {
        final short userAccess = getUserAccess();
        if (userAccess > accessLevel) throw new AccessDeniedExeption();
    }
}
