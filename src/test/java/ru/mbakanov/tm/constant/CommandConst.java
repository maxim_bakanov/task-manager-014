package ru.mbakanov.tm.constant;

public interface CommandConst {

    String HELP = "help";

    String ABOUT = "about";

    String VERSION = "version";

    String INFO = "info";

    String EXIT = "exit";

    String ARGUMENTS = "arguments";

    String COMMANDS = "commands";

    String TASK_LIST = "task-list";

    String TASK_CLEAR = "task-clear";

    String TASK_CREATE = "task-create";

    String PROJECT_LIST = "project-list";

    String PROJECT_CLEAR = "project-clear";

    String PROJECT_CREATE = "project-create";

    String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    String TASK_UPDATE_BY_ID = "task-update-by-id";

    String TASK_VIEW_BY_INDEX = "task-view-by-index";

    String TASK_VIEW_BY_NAME = "task-view-by-name";

    String TASK_VIEW_BY_ID = "task-view-by-id";

    String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    String TASK_REMOVE_BY_NAME = "task-remove-by-name";

    String TASK_REMOVE_BY_ID = "task-remove-by-id";

    String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    String PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    String PROJECT_VIEW_BY_NAME = "project-view-by-name";

    String PROJECT_VIEW_BY_ID = "project-view-by-id";

    String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    String LOGIN = "login";

    String LOGOUT = "logout";

    String REGISTRY = "registry";

    String CHANGE_USER_NAME = "user-change-name";

    String CHANGE_USER_EMAIL = "user-change-email";

    String CHANGE_USER_PASSWORD = "user-change-password";

    String SHOW_USERS = "user-list";

    String SHOW_USERS_INFO = "user-info-by-id";

    String SHOW_USER_INFO = "user-info";

}
