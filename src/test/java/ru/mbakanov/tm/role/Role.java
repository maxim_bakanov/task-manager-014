package ru.mbakanov.tm.role;

public enum Role {

    ADMIN("Администратор"),
    USER("Пользователь");

    private String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
