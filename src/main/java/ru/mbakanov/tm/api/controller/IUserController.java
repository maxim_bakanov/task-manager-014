package ru.mbakanov.tm.api.controller;

public interface IUserController {

    void changeName();

    void changeEmail();

    void changePassword();

    void showUsers();

    void showUsersInfo();

    void showUserInfo();

}
