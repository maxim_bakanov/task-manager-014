package ru.mbakanov.tm.api.controller;

public interface IAuthController {

    void login();

    void logout();

    void registry();

}
