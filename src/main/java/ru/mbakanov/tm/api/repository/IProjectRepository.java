package ru.mbakanov.tm.api.repository;

import ru.mbakanov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    void add(String userId, Project project);

    void remove(String userId, Project project);

    List<Project> findAll(String userId);

    void clean(String userId);

    Project findOneById(String userId, String id);

    Project findOneByName(String userId, String name);

    Project findOneByIndex(String userId, Integer index);

    Project removeOneById(String userId, String id);

    Project removeOneByName(String userId, String name);

    Project removeOneByIndex(String userId, Integer index);

}
