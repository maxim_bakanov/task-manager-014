package ru.mbakanov.tm.api.repository;

import ru.mbakanov.tm.entity.Task;

import java.util.List;

public interface  ITaskRepository {

    void add(String userId, Task task);

    void remove(String userId, Task task);

    List<Task> findAll(String userId);

    void clean(String userId);

    Task findOneById(String userId, String id);

    Task findOneByName(String userId, String name);

    Task findOneByIndex(String userId, Integer index);

    Task removeOneById(String userId, String id);

    Task removeOneByName(String userId, String name);

    Task removeOneByIndex(String userId, Integer index);

}
