package ru.mbakanov.tm.api.repository;

import ru.mbakanov.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    User updateNameById(String id, String firstName, String middleName, String lastName);

    User updateEmailById(String id, String email);

    User updatePasswordById(String id, String passwordHash);

}
