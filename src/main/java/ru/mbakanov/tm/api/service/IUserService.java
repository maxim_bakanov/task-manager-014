package ru.mbakanov.tm.api.service;

import ru.mbakanov.tm.entity.User;
import ru.mbakanov.tm.role.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String id);

    User findByLogin(String login);

    void updateName(String id, String firstName, String middleName, String lastName);

    void updateEmail(String id, String email);

    void updatePassword(String id, String passwordHash);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

}
