package ru.mbakanov.tm.api.service;

public interface IAuthService {

    String getUserId();

    void login(String login, String password);

    void registry(String login, String password, String email);

    void logout();

    short getUserAccess();

    void checkUserAccess(short accessLevel);

}
