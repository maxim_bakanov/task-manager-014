package ru.mbakanov.tm.bootstrap;

import ru.mbakanov.tm.api.controller.*;
import ru.mbakanov.tm.api.repository.ICommandRepository;
import ru.mbakanov.tm.api.repository.IProjectRepository;
import ru.mbakanov.tm.api.repository.ITaskRepository;
import ru.mbakanov.tm.api.repository.IUserRepository;
import ru.mbakanov.tm.api.service.*;
import ru.mbakanov.tm.constant.ArgumentConst;
import ru.mbakanov.tm.constant.CommandConst;
import ru.mbakanov.tm.controller.*;
import ru.mbakanov.tm.error.UnexpectedCommandExeption;
import ru.mbakanov.tm.repository.CommandRepository;
import ru.mbakanov.tm.repository.ProjectRepository;
import ru.mbakanov.tm.repository.TaskRepository;
import ru.mbakanov.tm.repository.UserRepository;
import ru.mbakanov.tm.role.Role;
import ru.mbakanov.tm.service.*;
import ru.mbakanov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final IAuthController authController = new AuthController(authService);

    private final IUserController userController = new UserController(userService, authService);

    public final ICommandRepository commandRepository = new CommandRepository();

    public final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService, authService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService, authService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService, authService);

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(final String[] args) {
        System.out.println("== Welcome to TASK MANAGER ==");
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.INFO:
                commandController.showInfo();
                break;
            case CommandConst.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConst.TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case CommandConst.TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CommandConst.TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case CommandConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CommandConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CommandConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case CommandConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConst.PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConst.PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConst.PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case CommandConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case CommandConst.LOGIN:
                authController.login();
                break;
            case CommandConst.LOGOUT:
                authController.logout();
                break;
            case CommandConst.REGISTRY:
                authController.registry();
                break;
            case CommandConst.CHANGE_USER_NAME:
                userController.changeName();
                break;
            case CommandConst.CHANGE_USER_EMAIL:
                userController.changeEmail();
                break;
            case CommandConst.CHANGE_USER_PASSWORD:
                userController.changePassword();
                break;
            case CommandConst.SHOW_USERS:
                userController.showUsers();
                break;
            case CommandConst.SHOW_USERS_INFO:
                userController.showUsersInfo();
                break;
            case CommandConst.SHOW_USER_INFO:
                userController.showUserInfo();
                break;
            case CommandConst.EXIT:
                commandController.exit();
                break;
            default: throw new UnexpectedCommandExeption(arg);
        }
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default: throw new UnexpectedCommandExeption(arg);
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}
