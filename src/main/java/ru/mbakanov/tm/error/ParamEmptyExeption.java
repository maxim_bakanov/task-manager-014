package ru.mbakanov.tm.error;

public class ParamEmptyExeption extends RuntimeException {

    public ParamEmptyExeption(String value){
        super("Error! " + value + " is empty...");
    }

}
