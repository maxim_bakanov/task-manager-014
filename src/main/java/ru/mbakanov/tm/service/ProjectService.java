package ru.mbakanov.tm.service;

import ru.mbakanov.tm.api.repository.IProjectRepository;
import ru.mbakanov.tm.error.*;
import ru.mbakanov.tm.entity.Project;

import java.util.List;

public class ProjectService implements ru.mbakanov.tm.api.service.IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (name == null || name.isEmpty()) throw new ParamEmptyExeption("Name");
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (name == null || name.isEmpty()) throw new ParamEmptyExeption("Name");
        if (description == null || description.isEmpty()) throw new DescriptionEmptyExeption();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    public void add(String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (project == null) throw new ObjectNotFoundExeption("Project");
        projectRepository.add(userId, project);
    }

    @Override
    public void remove(String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (project == null) throw new ObjectNotFoundExeption("Project");
        projectRepository.remove(userId, project);
    }

    @Override
    public List<Project> findAll(String userId) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        return projectRepository.findAll(userId);
    }

    @Override
    public void clean(String userId) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        projectRepository.clean(userId);
    }

    @Override
    public Project findOneById(String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (id == null || id.isEmpty()) throw new ParamEmptyExeption("Id");
        return projectRepository.findOneByName(userId, id);
    }

    @Override
    public Project findOneByName(String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (name == null || name.isEmpty()) throw new ParamEmptyExeption("Name");
        return projectRepository.findOneByName(userId, name);
    }

    @Override
    public Project findOneByIndex(String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Override
    public Project removeOneById(String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (id == null || id.isEmpty()) throw new ParamEmptyExeption("Id");
        return projectRepository.removeOneById(userId, id);
    }

    @Override
    public Project removeOneByName(String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (name == null || name.isEmpty()) throw new ParamEmptyExeption("Name");
        return projectRepository.removeOneByName(userId, name);
    }

    @Override
    public Project removeOneByIndex(String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        return projectRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Project updateOneById(String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (id == null || id.isEmpty()) throw new ParamEmptyExeption("Id");
        if (name == null || name.isEmpty()) throw new ParamEmptyExeption("Name");
        final Project project = findOneById(userId, id);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateOneByIndex(String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        if (name == null || name.isEmpty()) throw new ParamEmptyExeption("Name");
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ObjectNotFoundExeption("Project");
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
