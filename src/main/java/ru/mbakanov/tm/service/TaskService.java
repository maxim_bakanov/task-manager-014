package ru.mbakanov.tm.service;

import ru.mbakanov.tm.api.repository.ITaskRepository;
import ru.mbakanov.tm.api.service.ITaskService;
import ru.mbakanov.tm.error.*;
import ru.mbakanov.tm.entity.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository ITaskRepository) {
        this.taskRepository = ITaskRepository;
    }

    @Override
    public void create(String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (name == null || name.isEmpty()) throw new ParamEmptyExeption("Name");
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(userId, task);
    }

    @Override
    public void create(String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (name == null || name.isEmpty()) throw new ParamEmptyExeption("Name");
        if (description == null || description.isEmpty()) throw new DescriptionEmptyExeption();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
    }

    @Override
    public void add(String userId, final Task task) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (task == null) throw new ObjectNotFoundExeption("Task");
        taskRepository.add(userId, task);
    }

    @Override
    public void remove(String userId, final Task task) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (task == null) throw new ObjectNotFoundExeption("Task");
        taskRepository.remove(userId, task);
    }

    @Override
    public List<Task> findAll(String userId) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        return taskRepository.findAll(userId);
    }

    @Override
    public void clean(String userId) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        taskRepository.clean(userId);
    }

    @Override
    public Task findOneById(String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (id == null || id.isEmpty()) throw new ParamEmptyExeption("Id");
        return taskRepository.findOneByName(userId, id);
    }

    @Override
    public Task findOneByName(String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (name == null || name.isEmpty()) throw new ParamEmptyExeption("Name");
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    public Task findOneByIndex(String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        return taskRepository.findOneByIndex(userId, index);
    }

    @Override
    public Task removeOneById(String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (id == null || id.isEmpty()) throw new ParamEmptyExeption("Id");
        return taskRepository.removeOneById(userId, id);
    }

    @Override
    public Task removeOneByName(String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (name == null || name.isEmpty()) throw new ParamEmptyExeption("Name");
        return taskRepository.removeOneByName(userId, name);
    }

    @Override
    public Task removeOneByIndex(String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        return taskRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Task updateOneById(String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (id == null || id.isEmpty()) throw new ParamEmptyExeption("Id");
        if (name == null || name.isEmpty()) throw new ParamEmptyExeption("Name");
        final Task task = findOneById(userId, id);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateOneByIndex(String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User ID");
        if (index == null || index < 0) throw new IndexIncorrectExeption();
        if (name == null || name.isEmpty()) throw new ParamEmptyExeption("Name");
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        task.setName(name);
        task.setDescription(description);
        return task;
    }
}
