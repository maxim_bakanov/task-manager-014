package ru.mbakanov.tm.service;

import ru.mbakanov.tm.api.repository.IUserRepository;
import ru.mbakanov.tm.api.service.IUserService;
import ru.mbakanov.tm.entity.User;
import ru.mbakanov.tm.error.*;
import ru.mbakanov.tm.role.Role;
import ru.mbakanov.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new ParamEmptyExeption("Id");
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new ParamEmptyExeption("Login");
        return userRepository.findByLogin(login);
    }

    @Override
    public User removeUser(final User user) {
        if (user == null) throw new ObjectNotFoundExeption("User");
        return userRepository.removeUser(user);
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new ParamEmptyExeption("Id");
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new ParamEmptyExeption("Login");
        return userRepository.removeByLogin(login);
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new ParamEmptyExeption("Login");
        if (password == null || password.isEmpty()) throw new ParamEmptyExeption("Password");
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new ParamEmptyExeption("Login");
        if (password == null || password.isEmpty()) throw new ParamEmptyExeption("Password");
        if (email == null || email.isEmpty()) throw new ParamEmptyExeption("Email");
        final User user = create(login, password);
        if (user == null) throw new ObjectNotFoundExeption("User");
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new ParamEmptyExeption("Login");
        if (password == null || password.isEmpty()) throw new ParamEmptyExeption("Password");
        if (role == null) throw new ParamEmptyExeption("Role");
        final User user = create(login, password);
        if (user == null) throw new ObjectNotFoundExeption("User");
        user.setRole(role);
        return user;
    }

    @Override
    public void updateName(final String id, final String firstName, final String middleName, final String lastName) {
        if (id == null || id.isEmpty()) throw new ParamEmptyExeption("Id");
        final User user = findById(id);
        if (user.getFirstName() != null || !user.getFirstName().isEmpty()) {
            if (firstName == null || firstName.isEmpty()) throw new ParamEmptyExeption("First name");
        }
        if (user.getMiddleName() != null || !user.getMiddleName().isEmpty()) {
            if (middleName == null || middleName.isEmpty()) throw new ParamEmptyExeption("Middle name");
        }
        if (user.getLastName() != null || !user.getLastName().isEmpty()) {
            if (lastName == null || lastName.isEmpty()) throw new ParamEmptyExeption("Last name");
        }
        userRepository.updateNameById(id, firstName, middleName, lastName);
    }

    @Override
    public void updateEmail(final String id, final String email) {
        if (id == null || id.isEmpty()) throw new ParamEmptyExeption("Id");
        final User user = findById(id);
        if (user.getEmail() != null || !user.getEmail().isEmpty()) {
            if (email == null || email.isEmpty()) throw new ParamEmptyExeption("E-mail");
        }
        userRepository.updateEmailById(id, email);
    }

    @Override
    public void updatePassword(String id, String passwordHash) {
        if (id == null || id.isEmpty()) throw new ParamEmptyExeption("Id");
        if (passwordHash == null || passwordHash.isEmpty()) throw new ParamEmptyExeption("Password");
        userRepository.updatePasswordById(id, passwordHash);
    }
}
