package ru.mbakanov.tm.repository;

import ru.mbakanov.tm.api.repository.ICommandRepository;
import ru.mbakanov.tm.constant.ArgumentConst;
import ru.mbakanov.tm.constant.CommandConst;
import ru.mbakanov.tm.dto.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(
            CommandConst.HELP, ArgumentConst.HELP, "Show terminal`s commands.", (short) 2
    );

    public static final Command ABOUT = new Command(
            CommandConst.ABOUT, ArgumentConst.ABOUT, "Show developer info.", (short) 2
    );

    public static final Command VERSION = new Command(
            CommandConst.VERSION, ArgumentConst.VERSION, "Show application version.", (short) 2
    );

    public static final Command INFO = new Command(
            CommandConst.INFO, ArgumentConst.INFO, "Show system hardware info.", (short) 2
    );

    public static final Command EXIT = new Command(
            CommandConst.EXIT, null ,"Close application.", (short) 2
    );

    public static final Command COMMAND = new Command(
            CommandConst.COMMANDS, ArgumentConst.COMMANDS ,"Show program commands.", (short) 2
    );

    public static final Command ARGUMENT = new Command(
            CommandConst.ARGUMENTS, ArgumentConst.ARGUMENTS ,"Show program arguments.", (short) 2
    );

    private static final Command TASK_CREATE = new Command(
            CommandConst.TASK_CREATE, null ,"Create new task.", (short) 1
    );

    private static final Command TASK_CLEAR = new Command(
            CommandConst.TASK_CLEAR, null,"Remove all tasks.", (short) 1
    );

    private static final Command TASK_LIST = new Command(
            CommandConst.TASK_LIST, null,"Show all tasks.", (short) 1
    );

    private static final Command PROJECT_CREATE = new Command(
            CommandConst.PROJECT_CREATE, null ,"Create new project.", (short) 1
    );

    private static final Command PROJECT_CLEAR = new Command(
            CommandConst.PROJECT_CLEAR, null,"Remove all projects.", (short) 1
    );

    private static final Command PROJECT_LIST = new Command(
            CommandConst.PROJECT_LIST, null,"Show all projects.", (short) 1
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            CommandConst.TASK_UPDATE_BY_INDEX, null,"Update task by index.", (short) 1
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            CommandConst.TASK_UPDATE_BY_ID, null,"Update task by id.", (short) 1
    );

    private static final Command TASK_VIEW_BY_INDEX = new Command(
            CommandConst.TASK_VIEW_BY_INDEX, null,"Show task by index.", (short) 1
    );

    private static final Command TASK_VIEW_BY_NAME = new Command(
            CommandConst.TASK_VIEW_BY_NAME, null,"Show task by name.", (short) 1
    );

    private static final Command TASK_VIEW_BY_ID = new Command(
            CommandConst.TASK_VIEW_BY_ID, null,"Show task by id.", (short) 1
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            CommandConst.TASK_REMOVE_BY_INDEX, null,"Remove task by index.", (short) 1
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            CommandConst.TASK_REMOVE_BY_NAME, null,"Remove task by name.", (short) 1
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            CommandConst.TASK_REMOVE_BY_ID, null,"Remove task by id.", (short) 1
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            CommandConst.PROJECT_UPDATE_BY_INDEX, null,"Update project by index.", (short) 1
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            CommandConst.PROJECT_UPDATE_BY_ID, null,"Update project by id.", (short) 1
    );

    private static final Command PROJECT_VIEW_BY_INDEX = new Command(
            CommandConst.PROJECT_VIEW_BY_INDEX, null,"Show project by index.", (short) 1
    );

    private static final Command PROJECT_VIEW_BY_NAME = new Command(
            CommandConst.PROJECT_VIEW_BY_NAME, null,"Show project by name.", (short) 1
    );

    private static final Command PROJECT_VIEW_BY_ID = new Command(
            CommandConst.PROJECT_VIEW_BY_ID, null,"Show project by id.", (short) 1
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            CommandConst.PROJECT_REMOVE_BY_INDEX, null,"Remove project by index.", (short) 1
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            CommandConst.PROJECT_REMOVE_BY_NAME, null,"Remove project by name.", (short) 1
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            CommandConst.PROJECT_REMOVE_BY_ID, null,"Remove project by id.", (short) 1
    );

    private static final Command LOGIN = new Command(
            CommandConst.LOGIN, null,"Login user.", (short) 2
    );

    private static final Command LOGOUT = new Command(
            CommandConst.LOGOUT, null,"Logout user.", (short) 1
    );

    private static final Command REGISTRY = new Command(
            CommandConst.REGISTRY, null,"Registry user.", (short) 0
    );

    private static final Command CHANGE_USER_NAME = new Command(
            CommandConst.CHANGE_USER_NAME, null,"Change user names.", (short) 1
    );

    private static final Command CHANGE_USER_EMAIL = new Command(
            CommandConst.CHANGE_USER_EMAIL, null,"Change user e-mail.", (short) 1
    );

    private static final Command CHANGE_USER_PASSWORD = new Command(
            CommandConst.CHANGE_USER_PASSWORD, null,"Change user password.", (short) 1
    );

    private static final Command SHOW_USERS = new Command(
            CommandConst.SHOW_USERS, null,"Show all users.", (short) 0
    );

    private static final Command SHOW_USERS_INFO = new Command(
            CommandConst.SHOW_USERS_INFO, null,"Show user info by id.", (short) 0
    );

    private static final Command SHOW_USER_INFO = new Command(
            CommandConst.SHOW_USER_INFO, null,"Show user info.", (short) 1
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[] {
            HELP, ABOUT, VERSION, INFO, COMMAND, ARGUMENT, TASK_CREATE, TASK_CLEAR, TASK_LIST,
            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST, TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID, TASK_VIEW_BY_INDEX,
            TASK_VIEW_BY_NAME, TASK_VIEW_BY_ID, TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_NAME, TASK_REMOVE_BY_ID,
            PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID, PROJECT_VIEW_BY_INDEX, PROJECT_VIEW_BY_NAME, PROJECT_VIEW_BY_ID,
            PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_NAME, PROJECT_REMOVE_BY_ID, LOGIN, LOGOUT, REGISTRY,
            CHANGE_USER_NAME, CHANGE_USER_EMAIL, CHANGE_USER_PASSWORD, SHOW_USERS, SHOW_USERS_INFO, SHOW_USER_INFO, EXIT
    };

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public Command[] getCommands (short access, Command... values) {
        if (values == null || values.length == 0) return new Command[] {};
        final Command[] commands = new Command[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[i].getName() == null) continue;
            if (access <= values[i].getAccess()) {
                commands[index] = values[i];
                index++;
            }
        }
        return Arrays.copyOfRange(commands, 0, index);
    }

     public String[] getArgs (Command... values) {
        if (values == null || values.length == 0) return new String[] {};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public Command[] getCommands(short access) {
        return getCommands(access, TERMINAL_COMMANDS);
    }

    public String[] getArgs() {
        return ARGS;
    }

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
