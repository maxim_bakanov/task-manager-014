package ru.mbakanov.tm.repository;

import ru.mbakanov.tm.api.repository.ITaskRepository;
import ru.mbakanov.tm.error.ObjectNotFoundExeption;
import ru.mbakanov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private List<Task> tasks = new ArrayList<>();

    @Override
    public void add(String userId, Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void remove(String userId, Task task) {
        if (userId.equals(task.getUserId())) tasks.remove(task);
    }

    @Override
    public List<Task> findAll(String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clean(String userId) {
        final List<Task> tasks = findAll(userId);
        tasks.removeAll(tasks);
    }

    @Override
    public Task findOneById(String userId, final String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByName(String userId, final String name) {
        for (final Task task : tasks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(String userId, final Integer index) {
        return tasks.get(index-1);
    }

    @Override
    public Task removeOneById(String userId, final String id) {
        final Task task = findOneById(userId, id);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(String userId, final String name) {
        final Task task = findOneByName(userId, name);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneByIndex(String userId, final Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        remove(userId, task);
        return task;
    }
}
