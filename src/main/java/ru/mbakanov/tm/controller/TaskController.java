package ru.mbakanov.tm.controller;

import ru.mbakanov.tm.api.service.IAuthService;
import ru.mbakanov.tm.api.service.ITaskService;
import ru.mbakanov.tm.api.controller.ITaskController;
import ru.mbakanov.tm.error.ObjectNotFoundExeption;
import ru.mbakanov.tm.entity.Task;
import ru.mbakanov.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IAuthService authService;

    public TaskController(final ITaskService ITaskService, final IAuthService authService) {
        this.taskService = ITaskService;
        this.authService = authService;
    }

    final short commandAccessLevel = 1;

    @Override
    public void showTasks() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[LIST TASKS]");
        final String userId = authService.getUserId();
        final List<Task> tasks = taskService.findAll(userId);
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[CLEAR TASKS]");
        final String userId = authService.getUserId();
        taskService.clean(userId);
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[enter name:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[enter description:]");
        final String description = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        taskService.create(userId, name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskById() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[SHOW TASK]");
        System.out.println("[enter id]");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Task task = taskService.findOneById(userId, id);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        System.out.println("[OK]");
        showTask(task);
    }

    @Override
    public void showTaskByName() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[SHOW TASK]");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Task task = taskService.findOneByName(userId, name);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        System.out.println("[OK]");
        showTask(task);
    }

    @Override
    public void showTaskByIndex() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[SHOW TASK]");
        System.out.println("[enter index]");
        final Integer index = TerminalUtil.nextNumber();
        final String userId = authService.getUserId();
        final Task task = taskService.findOneByIndex(userId, index);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        System.out.println("[OK]");
        showTask(task);
    }

    private void showTask(final Task task) {
        authService.checkUserAccess(commandAccessLevel);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
    }

    @Override
    public void updateTaskById() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[UPDATE TASK]");
        System.out.println("[enter id]");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Task task = taskService.findOneById(userId, id);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[enter description]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdater = taskService.updateOneById(userId, id, name, description);
        if (taskUpdater == null) throw new ObjectNotFoundExeption("Updated task");
        System.out.println("[OK]");
        showTask(taskUpdater);
    }

    @Override
    public void updateTaskByIndex() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[UPDATE TASK]");
        System.out.println("[enter index]");
        final Integer index = TerminalUtil.nextNumber();
        final String userId = authService.getUserId();
        final Task task = taskService.findOneByIndex(userId, index);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[enter description]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdater = taskService.updateOneByIndex(userId, index, name, description);
        if (taskUpdater == null) throw new ObjectNotFoundExeption("Updated task");
        System.out.println("[OK]");
        showTask(taskUpdater);
    }

    @Override
    public void removeTaskById() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[REMOVE TASK]");
        System.out.println("[enter id]");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Task task = taskService.removeOneById(userId, id);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByName() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[REMOVE TASK]");
        System.out.println("[enter name]");
        final String name = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Task task = taskService.removeOneByName(userId, name);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[REMOVE TASK]");
        System.out.println("[enter index]");
        final Integer index = TerminalUtil.nextNumber();
        final String userId = authService.getUserId();
        final Task task = taskService.removeOneByIndex(userId, index);
        if (task == null) throw new ObjectNotFoundExeption("Task");
        else System.out.println("[OK]");
    }
}
