package ru.mbakanov.tm.controller;

import ru.mbakanov.tm.api.controller.ICommandController;
import ru.mbakanov.tm.api.service.IAuthService;
import ru.mbakanov.tm.api.service.ICommandService;
import ru.mbakanov.tm.constant.TerminalConst;
import ru.mbakanov.tm.dto.Command;
import ru.mbakanov.tm.role.Role;
import ru.mbakanov.tm.util.NumberUtil;

import java.util.Arrays;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    private final IAuthService authService;

    public CommandController(ICommandService commandService, IAuthService authService) {
        this.commandService = commandService;
        this.authService = authService;
    }

    final short commandAccessLevel = 2;

    public void showHelp() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[HELP]");
        final short access = authService.getUserAccess();
        final Command[] commands = commandService.getCommands(access);
        for (final Command command : commands) System.out.println(command);
    }

    public void showVersion() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[VERSION]");
        System.out.println(TerminalConst.VERSION_NUM);
    }

    public void showAbout() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[ABOUT]");
        System.out.println(TerminalConst.DEV_NAME);
        System.out.println(TerminalConst.DEV_EMAIL);
    }

    public void showInfo() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    public void showCommands() {
        authService.checkUserAccess(commandAccessLevel);
        final short access = authService.getUserAccess();
        final Command[] commands = commandService.getCommands(access);
        for (final Command command : commands) System.out.println(command);
    }

    public void showArguments() {
        authService.checkUserAccess(commandAccessLevel);
        final String[] arguments = commandService.getArgs();
        System.out.println(Arrays.toString(arguments));
    }

    public void exit() {
        authService.checkUserAccess(commandAccessLevel);
        System.exit(0);
    }

}
