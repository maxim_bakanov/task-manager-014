package ru.mbakanov.tm.controller;

import ru.mbakanov.tm.api.controller.IUserController;
import ru.mbakanov.tm.api.service.IAuthService;
import ru.mbakanov.tm.api.service.IUserService;
import ru.mbakanov.tm.entity.Task;
import ru.mbakanov.tm.entity.User;
import ru.mbakanov.tm.error.NonEqualExeption;
import ru.mbakanov.tm.error.ObjectNotFoundExeption;
import ru.mbakanov.tm.error.ParamEmptyExeption;
import ru.mbakanov.tm.util.HashUtil;
import ru.mbakanov.tm.util.TerminalUtil;

import java.util.List;

public class UserController implements IUserController {

    private final IUserService userService;

    private final IAuthService authService;

    public UserController(IUserService userService, IAuthService authService) {
        this.userService = userService;
        this.authService = authService;
    }

    final short commandAccessLevel = 1;

    @Override
    public void changeName() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[CHANGE NAMES]");
        final String userId = authService.getUserId();
        System.out.println("Enter first name: ");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter middle name: ");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("Enter last name: ");
        final String lastName = TerminalUtil.nextLine();
        userService.updateName(userId, firstName, middleName, lastName);
        System.out.println("[OK]");
    }

    @Override
    public void changeEmail() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[CHANGE E-MAIL]");
        final String userId = authService.getUserId();
        System.out.println("Enter e-mail: ");
        final String email = TerminalUtil.nextLine();
        userService.updateEmail(userId, email);
        System.out.println("[OK]");
    }

    @Override
    public void changePassword() {
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[CHANGE PASSWORD]");
        final String userId = authService.getUserId();
        System.out.println("Enter new password: ");
        final String password1 = TerminalUtil.nextLine();
        if (password1 == null || password1.isEmpty()) throw new ParamEmptyExeption("Password");
        System.out.println("Repeat new password: ");
        final String password2 = TerminalUtil.nextLine();
        if (!password1.equals(password2)) throw new NonEqualExeption("Passwords");
        final String passwordHash = HashUtil.salt(password1);
        userService.updatePassword(userId, passwordHash);
        System.out.println("[OK]");
    }

    @Override
    public void showUsers() {
        final short commandAccessLevel = 0;
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[LIST USERS]");
        final List<User> users = userService.findAll();
        int index = 1;
        for (User user : users) {
            System.out.println(index + ". " + user.getLogin() + ", user id:" + user.getId());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void showUsersInfo() {
        final short commandAccessLevel = 0;
        authService.checkUserAccess(commandAccessLevel);
        System.out.println("[USER INFO (admin)]");
        System.out.println("Enter user id");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new ParamEmptyExeption("User id");
        final User user = userService.findById(userId);
        if (user == null) throw new ObjectNotFoundExeption("User");
        System.out.println("Login: " + user.getLogin());
        System.out.println("ID: " + user.getId());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Middle name: " + user.getMiddleName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("E-mail: " + user.getEmail());
        System.out.println("Role: " + user.getRole());
    }

    @Override
    public void showUserInfo() {
        authService.checkUserAccess(commandAccessLevel);
        final String userId = authService.getUserId();
        System.out.println("[USER INFO]");
        final User user = userService.findById(userId);
        if (user == null) throw new ObjectNotFoundExeption("User");
        System.out.println("Login: " + user.getLogin());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Middle name: " + user.getMiddleName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("E-mail: " + user.getEmail());
        System.out.println("Role: " + user.getRole());
    }
}
